from exceptions import DetectStringException


class DecoratorHelpers:

    def validation_decorator(func):
        """
        Декоратор, реализующий запуск валидаторов
        :return:
        """

        def wrapper(*args, **kwargs):
            cls = args[0]
            cls.validation_chain(args[-1:])
            result = func(*args, **kwargs)
            return StringHelpers.str_formating(result)

        return wrapper


class ValidationHelpers:

    @staticmethod
    def is_both_int_or_float(numbers):
        """
        Метод проверки чисел в аргументах
        :param numbers:
        :return:
        """

        result = all([isinstance(i, int) or isinstance(i, float) for i in numbers])

        if not result:
            raise DetectStringException (
                f'String detected! Both numbers must be integer or float! Current value is: {numbers}'
            )

        return result

    @staticmethod
    def is_second_number_not_zero(number):
        """
        Проверка на наоль у второго аргумента
        :param number:
        :return:
        """
        result = number != 0

        if not result:
            raise DetectStringException(f'Zero detected! Second number must be more then 0')

        return result

    @staticmethod
    def is_any_float(numbers):
        """
        Проверка аргументов на float
        :param numbers:
        :return:
        """

        return any(isinstance(i, float) for i in numbers)

class StringHelpers:

    @staticmethod
    def str_formating(result):
        """
        Метод, реализующий заданное форматирование строки
        :param result:
        :return:
        """

        if isinstance(result, float) and f'{result}'.split('.')[-1] == '0':
            return f'{result}'.split('.')[0]

        return f'{result}'