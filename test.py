
import unittest

from py_calc import PyCalc


class TestPycalc(unittest.TestCase):

    def setUp(self):
        self.py_calc = PyCalc()

    def test_add(self):
        """
        Test add opperation
        :return:
        """

        self.assertEqual(self.py_calc.add(1, 1), '2')

    def test_minus(self):
        """
        Test minus opperation
        :return:
        """

        self.assertEqual(self.py_calc.minus(10, 1), '9')

    def test_multiply(self):
        """
        Test multiply opperation
        :return:
        """

        self.assertEqual(self.py_calc.multiply(2, 2), '4')

    def test_division(self):
        """
        Test division opperation
        :return:
        """

        self.assertEqual(self.py_calc.division(10, 2), '5')

    def test_pow(self):
        """
        Test pow opperation
        :return:
        """

        self.assertEqual(self.py_calc.pow(3, 3), '27')

    def tearDown(self):
        del self.py_calc


if __name__ == '__main__':
    unittest.main()