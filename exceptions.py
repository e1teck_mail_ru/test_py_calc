

class BasicException(Exception):
    """
    Базовое исключение
    """

    pass


class DetectStringException(BasicException):
    """
    Исключение при обнаружении строки
    """
    pass


class DetectZeroValueException(BasicException):
    """
    Исключчение при обнаружение нуля
    """
    pass