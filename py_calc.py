from operator import truediv
from helpers import DecoratorHelpers, ValidationHelpers


class PyCalc:
    """
    Свободная реализация калькулятора
    """

    def __init__(self):
        """
        Коструктор, инициализирующий цепочку валидации
        :param number_1:
        :param number_2:
        """

        self.validation_chain_list = self.init_validation_chain()

    def init_validation_chain(self):
        """
        Инициализация цепочки валидации
        :return:
        """
        return (ValidationHelpers.is_both_int_or_float,)

    def validation_chain(self, numbers):
        """
        Запуск цепочки валидации
        :param numbers:
        :return:
        """
        for chain_item in self.validation_chain_list:
            chain_item(numbers)


    @DecoratorHelpers.validation_decorator
    def add(self, number_1, number_2):
        """
        Метод, реализующий сложение двух чисел
        :param number_1:
        :param number_2:
        :return:
        """
        return number_1 + number_2

    @DecoratorHelpers.validation_decorator
    def minus(self, number_1, number_2):
        """
        Метод, реализующий вычитание двух чисел
        :param number_1:
        :param number_2:
        :return:
        """
        return number_1 - number_2

    @DecoratorHelpers.validation_decorator
    def multiply(self, number_1, number_2):
        """
        Метод, реализующий умножение двух чисел
        :param number_1:
        :param number_2:
        :return:
        """

        return number_1 * number_2

    @DecoratorHelpers.validation_decorator
    def division(self, number_1, number_2):
        """
        Метод, реализующий деление двух чисел
        :param number_1:
        :param number_2:
        :return:
        """

        ValidationHelpers.is_second_number_not_zero(number_2)

        return number_1 / number_2

    @DecoratorHelpers.validation_decorator
    def pow(self, number_1, number_2):
        """
        Метод, реализующий возведение в степень числа
        :param number_1:
        :param number_2:
        :return:
        """

        return number_1 ** number_2
