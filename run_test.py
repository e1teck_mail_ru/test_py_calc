import sys

from exceptions import DetectStringException, DetectZeroValueException
from py_calc import PyCalc


if __name__ == '__main__':
    _logger = lambda x: sys.stdout.write(f'{x}\n')

    calc = PyCalc()

    try:
        result = calc.add(10, 20)
    except (DetectStringException, DetectZeroValueException) as _err:
        _logger(_err)
    except Exception as _err:
        _logger(f'Undefined Exception {_err}')
    else:
        _logger(f'Add operation arg1 = 10, arg2 = 20, result = {result}')

    try:
        result = calc.minus(10, 20)
    except (DetectStringException, DetectZeroValueException) as _err:
        _logger(_err)
    except Exception as _err:
        _logger(f'Undefined Exception {_err}')
    else:
        _logger(f'Minus operation arg1 = 10, arg2 = 20, result = {result}')

    try:
        result = calc.pow(10, 20)
    except (DetectStringException, DetectZeroValueException) as _err:
        _logger(_err)
    except Exception as _err:
        _logger(f'Undefined Exception {_err}')
    else:
        _logger(f'Pow operation arg1 = 10, arg2 = 20, result = {result}')

    try:
        result = calc.pow(10, 'xdfgasd')
    except (DetectStringException, DetectZeroValueException) as _err:
        _logger(f'{_err}, coze current values 10, "xdfgasd"')
    except Exception as _err:
        _logger(f'Undefined Exception {_err}')
    else:
        _logger(result)

    try:
        result = calc.division(10, 2)
    except (DetectStringException, DetectZeroValueException) as _err:
        _logger(_err)
    except Exception as _err:
        _logger(f'Undefined Exception {_err}')
    else:
        _logger(f'Division operation arg1 = 10, arg2 = 2, result = {result}')

    try:
        result = calc.division(10, 0)
    except (DetectStringException, DetectZeroValueException) as _err:
        _logger(f'{_err}, coze current values 10, 0')
    except Exception as _err:
        _logger(f'Undefined Exception {_err}')
    else:
        _logger(result)
